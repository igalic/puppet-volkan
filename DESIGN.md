Teach Volkan some Puppet
========================

Reinforce some basics
---------------------

### Development Environment

So far, we have:

-   updated brew
-   installed [iTerm2](https://iterm2.com/downloads.html)! We now have emoji when running brew 😉
-   [automated brew update](https://tam7t.com/keeping-brew-up-to-date/)
-   installed rbenv & ruby-build ([from brew](https://alyssa.is/ruby-osx/))
-   rbenv installed a ruby
-   set that ruby, globally
-   gem install pry-doc pry-byebug pdk
-   brew install [neovim](https://neovim.io/) & [hub](https://hub.github.com/)

Now, our Development Environment is ready to be configured!

-   followed installation instructions on [vim-puppet](https://github.com/voxpupuli/vim-puppet).

Done 😉

### Git

-   learned how to create merge requests
    and [rebase](https://git-scm.com/book/en/v2/Git-Branching-Rebasing) our git
    repo against diverging remotes



### Clean Code

We've read thru a couple of modules `init.pp`

- [puppet-kafka](https://github.com/voxpupuli/puppet-kafka)
- [puppetlabs-postgresql](https://github.com/puppetlabs/puppetlabs-postgresql)

and to explain the [params](http://garylarizza.com/blog/2013/12/08/when-to-hiera/) [pattern](https://www.devco.net/archives/2013/12/09/the-problem-with-params-pp.php).

As home-work, I gave the following exercises:

- find a puppet module for a technology you know (well)
- read thru the source code (and documentation)
- write down everything that's unclear

Bonus (but it's okay if we do these things together)

- submit bug reports, if you find bugs
- submit pull requests, if you find solutions

### RSpec-puppet Tests


Focus on building Infra
-----------------------

### Profiles & Roles

### RSpec-server (integration) Tests

### Patch Upstream Modules

### Custom Facts

### Hiera

### Functions

### Iteration

Building our own Modules
------------------------
